import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View , ImageBackground,Button,paddingTop, TextInput} from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <ImageBackground source={require('./assets/grad.jpg')} style={styles.pic}/>
      <Text style ={styles.logi}>login form</Text>
      <View style={styles.formContainer}>
        <TextInput style={styles.input} placeholder='EMAIL'/> 
        <TextInput style={styles.input}  placeholder='PASSWORD'/>
        <Button title="login"/>



      </View>
      
      
     
     
      
      
    </View>
  );
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
 
   
    // alignItems: 'center',
    // justifyContent: 'center',
   
    

  },
  pic :{
    width :'100%',
    height :'100%',
    position : 'absolute',
  },
  logi :{
    position :'relative',
    fontSize: 50,
    color : 'white',
    marginTop :100,
    textAlign : 'center',
    textTransform : 'uppercase',


  },
  formContainer :{
    alignItems : 'center',
    justifyContent :'center',
    marginTop : 70,
    backgroundColor :"white",
    height:400,
    width:400,
    marginLeft:"35%",
   
    
  },
  names1 :
  {
    fontSize:40,
    marginRight:90,
    textTransform:'uppercase',
  },
  names2 :
  {
    fontSize:40,
    marginTop:10,
    textTransform:'uppercase',
  },
  input:{
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
